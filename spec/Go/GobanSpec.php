<?php

namespace spec\Go;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

final class GobanSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Go\Goban');
    }
}
