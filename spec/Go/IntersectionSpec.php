<?php

namespace spec\Go;

use InvalidArgumentException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Go\Intersection;

final class IntersectionSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Go\Intersection');
    }

    function it_is_not_occupied()
    {
        $this->shouldNotBeOccupied();
    }

    function it_doesnt_allow_invalid_state()
    {
        $this->beConstructedWith('wrong');
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_can_be_occupied()
    {
        $this->occupy(Intersection::OCCUPIED_BY_WHITE);
        $this->shouldBeOccupied();
    }

    function it_cant_be_occupied_again()
    {
        $this->beConstructedWith(Intersection::OCCUPIED_BY_BLACK);
        $this->shouldThrow(InvalidArgumentException::class)->duringOccupy(Intersection::OCCUPIED_BY_WHITE);
        $this->shouldBeOccupied();
    }
}
