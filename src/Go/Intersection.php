<?php

namespace Go;

use InvalidArgumentException;

/**
 * An intersection is a point on the goban where a horizontal line meets a vertical
 */
final class Intersection
{
    const EMPTY = 'empty';
    const OCCUPIED_BY_WHITE = 'white';
    const OCCUPIED_BY_BLACK = 'black';

    private $state;

    public function __construct($state = self::EMPTY)
    {
        if (!in_array($state, [self::EMPTY, self::OCCUPIED_BY_WHITE, self::OCCUPIED_BY_BLACK])) {
            throw new InvalidArgumentException('Invalid state');
        }

        $this->state = $state;
    }

    public function isOccupied()
    {
        return $this->state !== self::EMPTY;
    }

    public function occupy($color)
    {
        if (!in_array($color, [self::OCCUPIED_BY_WHITE, self::OCCUPIED_BY_BLACK])) {
            throw new InvalidArgumentException('Invalid color');
        }

        if ($this->isOccupied()) {
            throw new InvalidArgumentException('Our intersection can\'t be occupied again');
        }

        $this->state = $color;
    }
}
